import React, { useState, useContext } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Grid, Button } from '@mui/material';
import { PersonalInfoContext } from '../../App';
import { Pie } from 'react-chartjs-2';
import { Chart, ArcElement, Tooltip, Legend } from 'chart.js';
Chart.register(ArcElement, Tooltip, Legend);

export const ScoreCard = () => {
	const [ showScore, setShowScore ] = useState(false);
	const MyContext = useContext(PersonalInfoContext)!;
	const navigate = useNavigate();

	const data = {
		labels: [ 'Correct Answer', 'Incorrect Answer' ],
		datasets: [
			{
				label: 'Scorecard',
				data: [ MyContext.score, 5 - MyContext.score ],
				backgroundColor: [ 'rgb(0, 255, 0)', 'rgb(255, 0, 0)' ],
				hoverOffset: 4
			}
		]
	};

	const handleBack = (e: React.MouseEvent<HTMLButtonElement>) => {
		navigate(-1);
	};

	const handleShowScore = (e: React.MouseEvent<HTMLButtonElement>) => {
		setShowScore(true);
	};

	const handleBackHome = (e: React.MouseEvent<HTMLButtonElement>) => {
		navigate('/');
		MyContext.setscore(0);
		MyContext.setname('');
		MyContext.setgender('');
		MyContext.setlanguage('');
		MyContext.setcurrentQuestion(0);
	};

	return (
		<React.Fragment>
			{!showScore ? (
				<Grid container justifyContent="center" alignItems="center" direction="column">
					<Grid container item md={6} xs={12} justifyContent="center" alignItems="center" direction="column">
						<h2>Do you want to Submit?</h2>
					</Grid>
					<Grid item>
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={handleBack}>
							Back
						</Button>
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={handleShowScore}>
							Submit
						</Button>
					</Grid>
				</Grid>
			) : (
				<Grid container justifyContent="center" alignItems="center" direction="column">
					<Grid container item md={6} xs={12} justifyContent="center" alignItems="center" direction="column">
						<h2>Candidate: {MyContext.name} </h2>
						<h2>Score: {MyContext.score} </h2>
					</Grid>
					<Grid item>
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={handleBackHome}>
							Go To Home
						</Button>
					</Grid>
					<Grid item sx={{ marginY: '20px' }}>
						<Pie data={data} />
					</Grid>
				</Grid>
			)}
		</React.Fragment>
	);
};
