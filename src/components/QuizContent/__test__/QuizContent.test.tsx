import { screen, render, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter } from 'react-router-dom';
import { PersonalInfoContext } from '../../../App';
import { QuizContent } from '../QuizContent';
import { EnglishQuestions } from '../../QuestionBank/English';
import { HindiQuestions } from '../../QuestionBank/Hindi';

let questionBank = EnglishQuestions;
describe('Tests for QuizContent', () => {
	beforeEach(() => {
		render(
			<PersonalInfoContext.Provider
				value={{
					name: 'Arpit Jain',
					setname: jest.fn(),
					language: 'English',
					setlanguage: jest.fn(),
					gender: 'Man',
					setgender: jest.fn(),
					score: 0,
					setscore: jest.fn(),
					currentQuestion: 1,
					setcurrentQuestion: jest.fn()
				}}
			>
				<QuizContent />
			</PersonalInfoContext.Provider>,
			{ wrapper: BrowserRouter }
		);
	});

	it('[test-1]', () => {
		const button = screen.getByText(/Next/i);
		expect(button).toBeInTheDocument();
	});

	it('[test-2]', () => {
		const elem = screen.getByText('Candidate Name: Arpit Jain');
		expect(elem).toBeInTheDocument();
	});
	it('[test-3]', async () => {
		const button = screen.getByText(/Next/i);
		fireEvent.click(button);
		const button2 = await waitFor(() => screen.findByText(/Back/i), { timeout: 1000 });
		fireEvent.click(button2);
	});
});
