import React, { useContext, useState } from 'react';
import {
	Grid,
	Button,
	Typography,
	Stack,
	TextField,
	FormControl,
	FormLabel,
	RadioGroup,
	FormControlLabel,
	Radio,
	SelectChangeEvent,
	FormGroup,
	Checkbox
} from '@mui/material';
import { PersonalInfoContext } from '../../App';
import { useNavigate, useParams } from 'react-router-dom';
import { EnglishQuestions } from '../QuestionBank/English';
import { QuestionNumber } from '../QuestionNumber/QuestionNumber';
import { HindiQuestions } from '../QuestionBank/Hindi';

let questionBank: any;
let answers: any[] = [];

export const QuizContent = () => {
	const [ firstAnswer, setFirstAnswer ] = useState('');
	const [ thirdAnswer, setthirdAnswer ] = useState('');
	const [ truefalse, setTrueFalse ] = useState('');
	const [ fillblank, setFillBlank ] = useState('');
	const [ checkBoxState, setCheckBoxState ] = useState({
		CheckBoxA: false,
		CheckBoxB: false,
		CheckBoxC: false,
		CheckBoxD: false
	});
	const MyContext = useContext(PersonalInfoContext)!;
	const { LANGUAGE } = useParams<{ LANGUAGE: 'string' }>();
	const navigate = useNavigate();

	if (MyContext.name === '') {
		navigate('/');
	}

	if (MyContext.language === 'English') {
		questionBank = EnglishQuestions;
	} else if (MyContext.language === 'Hindi') {
		questionBank = HindiQuestions;
	}

	const { CheckBoxA, CheckBoxB, CheckBoxC, CheckBoxD } = checkBoxState;

	//Question-2
	const handleTrueFalse = (event: SelectChangeEvent): void => {
		setTrueFalse(event.target.value);
		answers[1] = event.target.value;
	};

	//Question-3
	const handleMatch = (event: React.ChangeEvent<HTMLInputElement>): void => {
		setthirdAnswer(event.target.value);
		answers[2] = event.target.value;
	};

	//Question-4
	const handleFillBlanks = (event: React.ChangeEvent<HTMLInputElement>): void => {
		setFillBlank(event.target.value);
		answers[3] = event.target.value;
	};

	//Question-5
	const handleCheckBox = (event: React.ChangeEvent<HTMLInputElement>) => {
		setCheckBoxState({
			...checkBoxState,
			[event.target.name]: event.target.checked
		});
		answers[4] = {
			...checkBoxState,
			[event.target.name]: event.target.checked
		};
	};

	//Buttons
	const ChangeQuestion = (e: React.MouseEvent<HTMLButtonElement>) => {
		MyContext.setcurrentQuestion((prev: number) => prev + 1);
	};

	const handleBack = (e: React.MouseEvent<HTMLButtonElement>) => {
		let current = MyContext.currentQuestion - 1;
		MyContext.setcurrentQuestion(current);
	};

	const handleSubmit = (e: React.MouseEvent<HTMLButtonElement>) => {
		navigate(`/quiz/${MyContext.name}/${MyContext.language}/confirmation`);

		if (MyContext.language === 'English') {
			if (firstAnswer === 'OptionB') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (thirdAnswer === 'A-3,B-4,C-1,D-2') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (truefalse === 'true') {
				MyContext.setscore((prev: number) => prev + 1);
			}
			if (fillblank === 'Mumbai' || fillblank === 'mumbai') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (CheckBoxB === true && CheckBoxC === true) {
				MyContext.setscore((prev: number) => prev + 1);
			}
		} else if (MyContext.language === 'Hindi') {
			if (firstAnswer === 'OptionA') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (thirdAnswer === 'A-3,B-4,C-1,D-2') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (truefalse === 'true') {
				MyContext.setscore((prev: number) => prev + 1);
			}
			if (fillblank === 'नील आर्मस्ट्रांग') {
				MyContext.setscore((prev: number) => prev + 1);
			}

			if (CheckBoxB === true && CheckBoxD === true) {
				MyContext.setscore((prev: number) => prev + 1);
			}
		}
	};

	return (
		<Grid container item justifyContent="center" alignItems="center" direction="column">
			<Grid container justifyContent="space-around" alignItems="center">
				<Grid item>
					<h4>Candidate Name: {MyContext.name}</h4>

					<h4>Score: {MyContext.score} </h4>
				</Grid>
				<Grid item sx={{ minWidth: '300px' }}>
					<QuestionNumber answers={answers} />
				</Grid>
			</Grid>

			<Grid
				container
				item
				md={6}
				xs={12}
				justifyContent="center"
				alignItems="center"
				direction="column"
				sx={{ padding: '50px' }}
			>
				{MyContext.currentQuestion === 0 ? (
					<React.Fragment>
						<Typography variant="h6" align="center" sx={{ width: '100%', marginBottom: '20px' }}>
							{questionBank[0].Question}
						</Typography>
						<Stack spacing={2} sx={{ width: '30%' }}>
							<Button
								onClick={() => {
									setFirstAnswer('OptionA');
									MyContext.setcurrentQuestion((prev: any) => prev + 1);
									answers[0] = 'OptionA';
								}}
								variant={firstAnswer === 'OptionA' ? 'contained' : 'outlined'}
								color={firstAnswer === 'OptionA' ? 'success' : 'primary'}
							>
								{questionBank[0].OptionA}
							</Button>
							<Button
								onClick={() => {
									setFirstAnswer('OptionB');
									MyContext.setcurrentQuestion((prev: any) => prev + 1);
									answers[0] = 'OptionB';
								}}
								variant={firstAnswer === 'OptionB' ? 'contained' : 'outlined'}
								color={firstAnswer === 'OptionB' ? 'success' : 'primary'}
							>
								{questionBank[0].OptionB}
							</Button>
							<Button
								onClick={() => {
									setFirstAnswer('OptionC');
									MyContext.setcurrentQuestion((prev: any) => prev + 1);
									answers[0] = 'OptionC';
								}}
								variant={firstAnswer === 'OptionC' ? 'contained' : 'outlined'}
								color={firstAnswer === 'OptionC' ? 'success' : 'primary'}
							>
								{questionBank[0].OptionC}
							</Button>
							<Button
								onClick={() => {
									setFirstAnswer('OptionD');
									MyContext.setcurrentQuestion((prev: any) => prev + 1);
									answers[0] = 'OptionD';
								}}
								variant={firstAnswer === 'OptionD' ? 'contained' : 'outlined'}
								color={firstAnswer === 'OptionD' ? 'success' : 'primary'}
							>
								{questionBank[0].OptionD}
							</Button>
						</Stack>
					</React.Fragment>
				) : null}
				{MyContext.currentQuestion === 1 ? (
					<React.Fragment>
						<Typography variant="h6" align="center" sx={{ width: '100%', marginBottom: '20px' }}>
							{questionBank[1].Question}
						</Typography>
						<FormControl>
							<RadioGroup row value={truefalse} onChange={handleTrueFalse}>
								<FormControlLabel value="true" label="true" control={<Radio />} />
								<FormControlLabel value="false" label="false" control={<Radio />} />
							</RadioGroup>
						</FormControl>
					</React.Fragment>
				) : null}
				{MyContext.currentQuestion === 2 ? (
					<React.Fragment>
						<Typography variant="h6" align="center" sx={{ width: '100%', marginBottom: '20px' }}>
							{questionBank[2].Question}
						</Typography>
						<Stack
							direction="row"
							spacing={2}
							sx={{
								width: '20%',
								padding: '20px',

								justifyContent: 'space-between'
							}}
						>
							<Stack>
								<h3>Country</h3>
								<Typography variant="subtitle1">A:{questionBank[2].Country[0]}</Typography>
								<Typography variant="subtitle1">B:{questionBank[2].Country[1]}</Typography>
								<Typography variant="subtitle1">C:{questionBank[2].Country[2]}</Typography>
								<Typography variant="subtitle1">D:{questionBank[2].Country[3]}</Typography>
							</Stack>
							<Stack>
								<h3>Capital</h3>
								<Typography variant="subtitle1">1:{questionBank[2].Capital[0]}</Typography>
								<Typography variant="subtitle1">2:{questionBank[2].Capital[1]}</Typography>
								<Typography variant="subtitle1">3:{questionBank[2].Capital[2]}</Typography>
								<Typography variant="subtitle1">4:{questionBank[2].Capital[3]}</Typography>
							</Stack>
						</Stack>
						<Stack direction="row">
							<TextField
								variant="outlined"
								size="small"
								margin="normal"
								value={thirdAnswer}
								onChange={handleMatch}
								helperText="eg: A-4, B-2"
								label="write answer here"
							/>
						</Stack>
					</React.Fragment>
				) : null}
				{MyContext.currentQuestion === 3 ? (
					<React.Fragment>
						<h3>Fill in the blank</h3>
						<Typography variant="h6" align="center" sx={{ width: '100%', marginBottom: '20px' }}>
							{
								<TextField
									variant="standard"
									size="small"
									margin="normal"
									value={fillblank}
									onChange={handleFillBlanks}
								/>
							}
							{questionBank[3].Question}
						</Typography>
					</React.Fragment>
				) : null}
				{MyContext.currentQuestion === 4 ? (
					<React.Fragment>
						<Typography variant="h6" align="center" sx={{ width: '100%', marginBottom: '20px' }}>
							{questionBank[4].Question}
						</Typography>
						<FormGroup>
							<FormControlLabel
								control={<Checkbox checked={CheckBoxA} onChange={handleCheckBox} name="CheckBoxA" />}
								label={questionBank[4].OptionA}
							/>
							<FormControlLabel
								control={<Checkbox checked={CheckBoxB} onChange={handleCheckBox} name="CheckBoxB" />}
								label={questionBank[4].OptionB}
							/>
							<FormControlLabel
								control={<Checkbox checked={CheckBoxC} onChange={handleCheckBox} name="CheckBoxC" />}
								label={questionBank[4].OptionC}
							/>
							<FormControlLabel
								control={<Checkbox checked={CheckBoxD} onChange={handleCheckBox} name="CheckBoxD" />}
								label={questionBank[4].OptionD}
							/>
						</FormGroup>
					</React.Fragment>
				) : null}

				<Grid item sx={{ marginTop: '20px' }}>
					{MyContext.currentQuestion !== 0 ? (
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={handleBack}>
							Back
						</Button>
					) : null}
					{MyContext.currentQuestion < 4 ? (
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={ChangeQuestion}>
							Next
						</Button>
					) : (
						<Button sx={{ marginX: '20px' }} variant="contained" onClick={handleSubmit}>
							Submit
						</Button>
					)}
				</Grid>
			</Grid>
		</Grid>
	);
};
