import '@testing-library/jest-dom';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter } from 'react-router-dom';
import { HomePage } from '../HomePage';
import { PersonalInfoContext } from '../../../App';

describe('Tests for HomePage Component', () => {
	beforeEach(() => {
		render(
			<PersonalInfoContext.Provider
				value={{
					name: 'Arpit Jain',
					setname: jest.fn(),
					language: 'Enlgish',
					setlanguage: jest.fn(),
					gender: 'Man',
					setgender: jest.fn(),
					score: 0,
					setscore: jest.fn(),
					currentQuestion: 0,
					setcurrentQuestion: jest.fn()
				}}
			>
				<HomePage />
			</PersonalInfoContext.Provider>,
			{ wrapper: BrowserRouter }
		);
	});
	it('[Test-1]', () => {
		const inputelem = screen.getByLabelText(/Enter Name/i);
		expect(inputelem).toBeInTheDocument();
	});

	it('[Test-2]', () => {
		const button = screen.getByText('Submit');
		expect(button).toBeInTheDocument();
	});
});
