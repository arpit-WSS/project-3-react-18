import {
	FormControl,
	FormControlLabel,
	Grid,
	InputLabel,
	MenuItem,
	TextField,
	Typography,
	Select,
	SelectChangeEvent,
	Button
} from '@mui/material';
import React, { useContext } from 'react';
import { PersonalInfoContext } from '../../App';
import { useNavigate } from 'react-router-dom';

export const HomePage = () => {
	const MyContext = useContext(PersonalInfoContext)!;
	const navigate = useNavigate();
	const handleName = (e: React.ChangeEvent<HTMLInputElement>) => {
		MyContext.setname(e.target.value);
	};

	const handleGender = (e: SelectChangeEvent) => {
		MyContext.setgender(e.target.value);
	};

	const handleLanguage = (e: SelectChangeEvent) => {
		MyContext.setlanguage(e.target.value);
	};

	const handleSubmit = (e: React.MouseEvent<HTMLButtonElement>) => {
		if (MyContext.name === '' || MyContext.language === '' || MyContext.gender === '') {
			alert('PLEASE FILL ALL THE DETAILS');
		} else {
			navigate(`/quiz/${MyContext.name}`);
		}
	};

	return (
		<Grid container item md={12} justifyContent="center" alignItems="center" direction="column">
			<Grid item>
				<Typography
					variant="h4"
					color="blueviolet"
					align="center"
					sx={{ width: '100%', fontWeight: '500', marginY: '30px' }}
				>
					Welcome to My Quiz
				</Typography>
			</Grid>
			<Grid
				container
				item
				justifyContent="center"
				alignItems="center"
				direction="column"
				sx={{
					border: '1px solid black',
					width: '550px',
					marginY: '30px',
					padding: '30px',
					borderRadius: '20px',
					boxShadow: '20px 20px 30px 2px gray'
				}}
			>
				<Grid item>
					<TextField
						value={MyContext.name}
						onChange={handleName}
						variant="outlined"
						size="small"
						margin="normal"
						label="Enter Name"
					/>
				</Grid>
				<Grid item>
					<FormControl sx={{ m: 1, width: '200px' }}>
						<InputLabel>Gender</InputLabel>
						<Select label="Gender" value={MyContext.gender} onChange={handleGender}>
							<MenuItem value={'Man'}>Man</MenuItem>
							<MenuItem value={'Woman'}>Woman</MenuItem>
							<MenuItem value={'Others'}>Others</MenuItem>
						</Select>
					</FormControl>
				</Grid>
				<Grid item>
					<FormControl sx={{ m: 1, width: '200px' }}>
						<InputLabel>Language</InputLabel>
						<Select label="Language" value={MyContext.language} onChange={handleLanguage}>
							<MenuItem value={'English'}>English</MenuItem>
							<MenuItem value={'Hindi'}>Hindi</MenuItem>
							<MenuItem value={'Others'}>Others</MenuItem>
						</Select>
					</FormControl>
				</Grid>
				<Grid item>
					<Typography
						variant="body2"
						color="red"
						align="center"
						sx={{ width: '100%', fontWeight: '500', marginY: '30px' }}
					>
						* For the demo app choose language as English only
					</Typography>
				</Grid>
				<Grid item>
					<Button variant="contained" onClick={handleSubmit}>
						Submit
					</Button>
				</Grid>
			</Grid>
		</Grid>
	);
};
