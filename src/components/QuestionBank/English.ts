export const EnglishQuestions = [{
        'Question': 'How many states does india have?',
        'OptionA': 28,
        'OptionB': 29,
        'OptionC': 26,
        'OptionD': 27,
        'Correct': 'OptionB'
    },
    {
        'Question': 'Roger Binny played for India. True or False',
        'Correct': 'true'
    },
    {
        'Question': 'Match the following country with their capital',
        'Country': ['Brazil', 'India', 'America', 'China'],
        'Capital': [ 'Washington DC', 'Beijing', 'Brazilia', 'Delhi'],
        'Answer': 'A-3, B-4, C-1, D-2'
      
    },
    {
        'Question': 'is the capital of Maharastra',
        'Correct': 'Mumbai'
    },
    {
        'Question': 'Which of the following is/are Pokemon(s)?',
        'OptionA': 'Dinasour',
        'OptionB': 'Bulbasour',
        'OptionC': 'Pikachu',
        'OptionD': 'Hippo',
        'Correct': ['OptionB', 'OptionC']
    },
]
