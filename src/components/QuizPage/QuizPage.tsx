import { Button, Grid, Typography } from '@mui/material';
import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { PersonalInfoContext } from '../../App';

export const QuizPage = () => {
	const MyContext = useContext(PersonalInfoContext)!;
	const navigate = useNavigate();

	if (MyContext.name === '') {
		navigate('/');
	}

	const handleSubmit = () => {
		if (MyContext.language === 'English') {
			navigate(`/quiz/${MyContext.name}/${MyContext.language}`);
		} else if (MyContext.language === 'Hindi') {
			navigate(`/quiz/${MyContext.name}/${MyContext.language}`);
		} else if (MyContext.language === 'Others') {
			navigate(`/quiz/${MyContext.name}/${MyContext.language}`);
		}
	};

	return (
		<Grid
			container
			item
			md={12}
			justifyContent="center"
			alignItems="center"
			direction="column"
			sx={{ minHeight: '696px' }}
		>
			<Grid
				item
				p={2}
				sx={{
					border: '1px solid black',
					width: '550px',
					marginY: '30px',
					padding: '30px',
					borderRadius: '20px',
					boxShadow: '5px 5px 20px 2px gray'
				}}
			>
				<Typography variant="h4" align="center" sx={{ width: '100%', marginBottom: '20px', fontWeight: 500 }}>
					Hello, {MyContext.name}
				</Typography>
				<Typography variant="body1" align="center">
					Quiz Details: <br />
				</Typography>
				<Typography variant="body1" align="center">
					{'* Language:'} <span style={{ color: 'green', fontWeight: 600 }}>{MyContext.language}</span>
				</Typography>
				<Typography variant="body1" align="center">
					* Questions: <span style={{ color: 'green', fontWeight: 600 }}>5</span>
				</Typography>
				<Typography variant="body1" align="center">
					* Time: <span style={{ color: 'green', fontWeight: 600 }}>10 min</span>
				</Typography>
			</Grid>
			<Grid item m={2}>
				<Button variant="contained" onClick={handleSubmit}>
					Start Quiz
				</Button>
			</Grid>
		</Grid>
	);
};
