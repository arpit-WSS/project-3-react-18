import '@testing-library/jest-dom';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { BrowserRouter } from 'react-router-dom';
import { PersonalInfoContext } from '../../../App';
import { QuizPage } from '../QuizPage';

describe('Tests for QuizPage Components', () => {
	beforeEach(() => {
		render(
			<PersonalInfoContext.Provider
				value={{
					name: 'Arpit Jain',
					setname: jest.fn(),
					language: 'Enlgish',
					setlanguage: jest.fn(),
					gender: 'Man',
					setgender: jest.fn(),
					score: 0,
					setscore: jest.fn(),
					currentQuestion: 0,
					setcurrentQuestion: jest.fn()
				}}
			>
				<QuizPage />
			</PersonalInfoContext.Provider>,
			{ wrapper: BrowserRouter }
		);
	});

	it('[Test-1]', () => {
		const button = screen.getByText('Start Quiz');
		expect(button).toBeInTheDocument();
	});

	it('[Test-1]', () => {
		const button = screen.getByText('Start Quiz');
		fireEvent.click(button)!;
	});
});
