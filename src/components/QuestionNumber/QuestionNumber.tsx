import React, { useContext, useState } from 'react';
import { Grid, Button, Stack, TextField } from '@mui/material';
import { PersonalInfoContext } from '../../App';

export const QuestionNumber = (props: { answers: any[] }) => {
	const MyContext = useContext(PersonalInfoContext)!;
	// const [ buttonColor, setButtoncolor ] = useState('Button1');
	console.log(props.answers);
	return (
		<Stack spacing={{ xs: 1, sm: 2, md: 4 }} direction="row">
			<Button
				onClick={() => {
					MyContext.setcurrentQuestion(0);
				}}
				size="small"
				variant="contained"
				color={props.answers[0] ? 'error' : MyContext.currentQuestion === 0 ? 'secondary' : 'inherit'}
				sx={{ borderRadius: '50px' }}
			>
				1
			</Button>
			<Button
				onClick={() => {
					MyContext.setcurrentQuestion(1);
				}}
				size="small"
				variant="contained"
				color={props.answers[1] ? 'error' : MyContext.currentQuestion === 1 ? 'secondary' : 'inherit'}
				sx={{ borderRadius: '50px', width: '5px' }}
			>
				2
			</Button>
			<Button
				onClick={() => {
					MyContext.setcurrentQuestion(2);
				}}
				size="small"
				variant="contained"
				color={props.answers[2] ? 'error' : MyContext.currentQuestion === 2 ? 'secondary' : 'inherit'}
				sx={{ borderRadius: '50px' }}
			>
				3
			</Button>
			<Button
				onClick={() => {
					MyContext.setcurrentQuestion(3);
				}}
				size="small"
				variant="contained"
				color={props.answers[3] ? 'error' : MyContext.currentQuestion === 3 ? 'secondary' : 'inherit'}
				sx={{ borderRadius: '50px' }}
			>
				4
			</Button>
			<Button
				onClick={() => {
					MyContext.setcurrentQuestion(4);
				}}
				size="small"
				variant="contained"
				color={props.answers[4] ? 'error' : MyContext.currentQuestion === 4 ? 'secondary' : 'inherit'}
				sx={{ borderRadius: '50px' }}
			>
				5
			</Button>
		</Stack>
	);
};
