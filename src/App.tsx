import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useState, createContext } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AppBarComponent } from './components/AppBarComponent/AppBarComponent';
import { HomePage } from './components/HomePage/HomePage';
import { QuizPage } from './components/QuizPage/QuizPage';
import { QuizContent } from './components/QuizContent/QuizContent';
import { ScoreCard } from './components/Scorecard/ScoreCard';

type Context = {
	name: string;
	language: string;
	gender: string;
	setname: React.Dispatch<React.SetStateAction<string>>;
	setgender: React.Dispatch<React.SetStateAction<string>>;
	setlanguage: React.Dispatch<React.SetStateAction<string>>;
	score: number;
	setscore: React.Dispatch<React.SetStateAction<number>>;
	setcurrentQuestion: React.Dispatch<React.SetStateAction<number>>;
	currentQuestion: number;
};

// type Context = string | number | null | React.Dispatch<React.SetStateAction<string | number | null>>;

export const PersonalInfoContext = createContext<Context | null>(null);
function App() {
	const [ name, setname ] = useState('');
	const [ language, setlanguage ] = useState('');
	const [ gender, setgender ] = useState('');
	const [ score, setscore ] = useState(0);
	const [ currentQuestion, setcurrentQuestion ] = useState(0);

	return (
		<BrowserRouter>
			<AppBarComponent />
			<PersonalInfoContext.Provider
				value={{
					name,
					setname,
					language,
					setlanguage,
					gender,
					setgender,
					score,
					setscore,
					currentQuestion,
					setcurrentQuestion
				}}
			>
				<Routes>
					<Route path="/" element={<HomePage />} />
					<Route path="/quiz/:NAME" element={<QuizPage />} />
					<Route path="/quiz/:NAME/:LANGUAGE" element={<QuizContent />} />
					<Route path="/quiz/:NAME/:LANGUAGE/confirmation" element={<ScoreCard />} />
				</Routes>
			</PersonalInfoContext.Provider>
		</BrowserRouter>
	);
}

export default App;
